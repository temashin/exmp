<?php

namespace App\Tests\Payments\Managers;

use App\Configuration\BudgetAccData;
use App\ecc\core\DB\DB;
use App\ElectronicWallet\TransferMoneyManager;
use App\Entity\AbstractPsRetPay;
use App\Entity\PsRetPayFromPay;
use App\Entity\PsRetPayFromWallet;
use App\integrations\Egz\Payments\RetPayManager;
use App\Payments\Managers\ManagerFabric;
use App\Payments\Managers\OutgoingPaymentFromPayManager;
use App\Payments\Managers\OutgoingPaymentFromWalletManager;
use App\Repository\PsAccRepository;
use App\Repository\PsPayRepository;
use App\Repository\PsReservedRepository;
use App\Repository\RefDirectRepository;
use App\Repository\RefPayStRepository;
use App\Repository\Refs\RefReservedStRepository;
use App\Repository\Refs\RefReservedTypeRepository;
use App\Repository\Refs\RefUnblockReasonRepository;
use App\Tests\Core\Core;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\Stub\ReturnStub;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ManagerFabricTest extends KernelTestCase {

    /**
     * @var KernelInterface
     */
    protected static $kernel;

    /**
     * @var Core
     */
    private $core;

    /**
     * @var ConsoleLogger
     */
    private $logger;

    protected function setUp(): void
    {
        self::$kernel = self::bootKernel();
        $this->core = self::$container->get(Core::class);
        $this->logger = self::$container->get(LoggerInterface::class);
    }
    
    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function ManagerTypeFromPay() {

        $managerFabric = $this->getManagerFabric();
        $manager = $managerFabric->getManagerForPayment($this->getPsRetPayFromPay());

        $this->assertInstanceOf(OutgoingPaymentFromPayManager::class, $manager);
    }

    /**
     * @test
     */
    public function ManagerTypeFromWallet() {

        $managerFabric = $this->getManagerFabric();
        $manager = $managerFabric->getManagerForPayment($this->getPsRetPayFromWallet());

        $this->assertInstanceOf(OutgoingPaymentFromWalletManager::class, $manager);
    }

    /**
     * @return PsRetPayFromPay
     */
    private function getPsRetPayFromPay()
    {
        return (new PsRetPayFromPay());
    }

    private function getPsRetPayFromWallet()
    {
        return (new PsRetPayFromWallet());
    }

    /**
     * @return ManagerFabric
     */
    private function getManagerFabric() {
        return new ManagerFabric(
             self::$container->get(EntityManagerInterface::class)
            ,self::$container->get(PsAccRepository::class)
            ,self::$container->get(PsPayRepository::class)
            ,self::$container->get(DB::class)
            ,self::$container->get(ValidatorInterface::class)
            ,self::$container->get(RefPayStRepository::class)
            ,self::$container->get(RefDirectRepository::class)
            ,self::$container->get(RetPayManager::class)
            ,self::$container->get(PsReservedRepository::class)
            ,self::$container->get(TransferMoneyManager::class)
            ,self::$container->get(RefReservedTypeRepository::class)
            ,self::$container->get(RefReservedStRepository::class)
            ,self::$container->get(RefUnblockReasonRepository::class)
            ,self::$container->get(BudgetAccData::class)
        );
    }


}
