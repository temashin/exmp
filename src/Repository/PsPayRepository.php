<?php

namespace App\Repository;

use App\Entity\PsPay;
use Closure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PsPay|null find($id, $lockMode = null, $lockVersion = null)
 * @method PsPay|null findOneBy(array $criteria, array $orderBy = null)
 * @method PsPay[]    findAll()
 * @method PsPay[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PsPayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PsPay::class);
    }

    public function getSorterByPayDay(): Closure
    {
        return function (PsPay $first,PsPay $second){return $first->getPayDay()<=>$second->getPayDay();};
    }


    /**
     * @param array $referencesAr
     * @return PsPay[]
     */
    public function findIdsForDuplicate(array $referencesAr):array
    {
//        $qb = $this->createQueryBuilder('pp');
//        $qb->select('pp.reference')->where($qb->expr()->in('pp.reference',$referencesAr));
//        return $qb->getQuery()->execute();
        return $this->findBy(['reference'=>$referencesAr]);

    }

    /**
     * @param string $worker_id
     * @return PsPay|null
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getPaymentFromQueue(string $worker_id):?PsPay
    {
        $counter = 0;

        while($counter < 3)
        {
            $stmt = $this->getEntityManager()->getConnection()->executeQuery(
                'select Get_Pay_From_Queue(:worker_id) as result from dual'
                ,['worker_id'=>$worker_id]
            );

            $res = $stmt->fetch();

            if(isset($res['RESULT']) && (int)$res['RESULT'] > 0) {
                return $this->find($res['RESULT']);
            }
            $counter++;
            sleep(2);
        }
        return null;
    }
}
