<?php

namespace App\Entity;

use App\ecc\core\DB\SoapDateTime;
use App\ecc\core\Exceptions\UnhandledException;
use App\ecc\core\GetterSetterAccessor;
use App\ecc\core\SetStateTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Payments\MoneySource;
use DateTimeInterface;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * PsPay
 *
 * @ORM\Table(name="PS_PAY", uniqueConstraints={@ORM\UniqueConstraint(name="ps_pay_uk", columns={"REFERENCE"})}, indexes={@ORM\Index(name="ps_pay_fk_ecc_acc", columns={"ECC_ACC_ID"}), @ORM\Index(name="ps_pay_fk_direct", columns={"DIRECT"}), @ORM\Index(name="ps_pay_fk_st", columns={"STATUS_ID"})})
 * @ORM\Entity(repositoryClass="App\Repository\PsPayRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PsPay implements MoneySource
{
    use GetterSetterAccessor;
    use SetStateTrait;

    const STATUS_DELETED = "DELETED";
    const STATUS_CREATED = "CREATED";

    /**
     * Не обработано
     */
    const JOB_STATUS_NOT_HANDLED = 1;

    /**
     * В обработке
     */
    const JOB_STATUS_WORKING = 2;
    /**
     * @var int
     * Успешно обработано
     */
    const JOB_STATUS_SUCCESS = 3;

    /**
     * @var int
     * Ошибка обработки
     */
    public const JOB_STATUS_ERROR = 4;

    /**
     * var int
     * выполнение отложено
     */
    const JOB_STATUS_DELAYED = 5;

    /**
     * var int
     * платёж удален
     */
    const JOB_STATUS_DELETED = 6;

    /**
     * Ожидает подтверждения
     */
    const JOB_STATUS_NOT_APPROVED = 7;

    /**
     * Отклонено бухгалтерией
     */
    const JOB_STATUS_REJECTED = 8;


    /**
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"comment"="Идентификатор"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="PS_PAY_ID_seq", allocationSize=1, initialValue=1)
     */
    private int $id;

    /**
     * @ORM\Column(name="internal_id", type="integer", length=10, nullable=false, options={"fixed"=true,"comment"=""})
     */
    private int $internal_id;

    /**
     * @return int
     */
    public function getInternalId(): int
    {
        return $this->internal_id;
    }

    /**
     * @param int $internal_id
     * @return PsPay
     */
    public function setInternalId(int $internal_id): PsPay
    {
        $this->internal_id = $internal_id;
        return $this;
    }

    /**
     * @ORM\Column(name="PAY_DAY", type="datetime", nullable=false, options={"comment"="Дата проведения платежа"})
     */
    private \DateTimeInterface $pay_day;

    /**
     * @ORM\Column(name="REFERENCE", type="string", length=16, nullable=false, options={"comment"="Референс платежа"})
     */
    private string $reference;

    /**
     * @ORM\Column(name="SENDER_IDN", type="string", length=12, nullable=false, options={"comment"="БИН/ИИН отправителя"})
     */
    private string $senderIdn;

    /**
     * @ORM\Column(name="SENDER_NAME", type="string", length=60, nullable=false, options={"comment"="Наименование отправителя"})
     */
    private string $sender_name;

    /**
     * @ORM\Column(name="SENDER_BIK", type="string", length=11, nullable=false, options={"comment"="БИК банка отправителя"})
     */
    private string $sender_bik;

    /**
     * @ORM\Column(name="SENDER_IBAN", type="string", length=20, nullable=false, options={"comment"="ИИК отправителя"})
     */
    private string $sender_iban;

    /**
     * @ORM\Column(name="RECIPIENT_IDN", type="string", length=12, nullable=false, options={"comment"="БИН/ИИН получателя"})
     */
    private string $recipient_idn;

    /**
     * @ORM\Column(name="RECIPIENT_NAME", type="string", length=60, nullable=false, options={"comment"="Наименование получателя"})
     */
    private string $recipient_name;

    /**
     * @ORM\Column(name="RECIPIENT_BIK", type="string", length=11, nullable=false, options={"comment"="БИК банка получателя"})
     */
    private string $recipient_bik;

    /**
     * @ORM\Column(name="RECIPIENT_IBAN", type="string", length=20, nullable=false, options={"comment"="ИИК получателя"})
     */
    private string $recipientIban;

    /**
     * @ORM\Column(name="AMOUNT", type="decimal", precision=19, scale=2, nullable=false, options={"comment"="Сумма платежа"})
     */
    private float $amount;

    /**
     * @ORM\Column(name="DOC_NUM", type="string", length=9, nullable=false, options={"comment"="Номер платежного поручения"})
     */
    private string $doc_num;

    /**
     * @ORM\Column(name="DOC_DATE", type="datetime", nullable=false, options={"comment"="Дата платежного поручения"})
     */
    private \DateTimeInterface $doc_date;

    /**
     * @ORM\Column(name="DOC_ASSIGN", type="string", length=490, nullable=true, options={"comment"="Назначение платежа"})
     */
    private ?string $doc_assign;

    /**
     * @ORM\Column(name="KNP", type="string", length=3, nullable=false, options={"fixed"=true,"comment"="Код назначения платежа (КНП)"})
     */
    private string $knp;

    /**
     * @ORM\ManyToOne(targetEntity="RefDirect")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DIRECT", referencedColumnName="CODE")
     * })
     */
    private RefDirect $direct;
    /**
     * @ORM\ManyToOne(targetEntity="RefEccAcc")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ECC_ACC_ID", referencedColumnName="ID")
     * })
     */
    private ?RefEccAcc $eccAcc;

    /**
     * @ORM\ManyToOne(targetEntity="RefPaySt")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="STATUS_ID", referencedColumnName="ID")
     * })
     */
    private RefPaySt $refStatus;

    /**
     * @ORM\Column(name="job_worker_id", type="string", length=32, nullable=true, options={"fixed"=true,"comment"=""})
     */
    private ?string $job_worker_id;

    /**
     * @ORM\Column(name="job_status", type="integer", length=2, nullable=false, options={"fixed"=true,"comment"=""})
     */
    private ?int $job_status;

    /**
     * @ORM\Column(name="job_start_time", type="datetime",  nullable=true, options={"fixed"=true,"comment"=""})
     */
    private ?\DateTimeInterface $job_start_time;

    /**
     * @var ArrayCollection|PersistentCollection
     * @ORM\OneToMany(targetEntity="PsPayErrorLog", mappedBy="pay")
     */
    private $errors;

    /**
     * @ORM\Column(name="approved_user", type="integer")
     */
    private ?int $approved_user;

    /**
     * @ORM\Column(name="approved_date", type="datetime")
     */
    private ?\DateTimeInterface $approved_date;

    /**
     * @ORM\Column(name="fakt_sender_idn", type="string")
     */
    private string $fakt_sender_idn;

    /***
     * @ORM\Column(name="crdate", type="datetime",  nullable=true, options={"fixed"=true,"comment"="Дата создания записи"})
     */
    private ?\DateTimeInterface $crdate;

    /**
     * @ORM\Column(name="created_status", type="string", nullable=false, options={"comment"="Статус создания записи"})
     * @Assert\Choice({PsPay::STATUS_DELETED,PsPay::STATUS_CREATED})
     */
    private string $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPayDay(): ?\DateTimeInterface
    {
        return $this->pay_day;
    }

    public function setPayDay(\DateTimeInterface $pay_day): PsPay
    {
        $this->pay_day = $pay_day;
        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): PsPay
    {
        $this->reference = $reference;

        return $this;
    }

    public function getSenderIdn(): ?string
    {
        return $this->senderIdn;
    }

    public function setSenderIdn(string $senderIdn): PsPay
    {
        $this->senderIdn = $senderIdn;

        return $this;
    }

    public function getSenderName(): ?string
    {
        return $this->sender_name;
    }

    public function setSenderName(string $senderName): PsPay
    {
        $this->sender_name = $senderName;

        return $this;
    }

    public function getSenderBik(): ?string
    {
        return $this->sender_bik;
    }

    public function setSenderBik(string $senderBik): PsPay
    {
        $this->sender_bik = $senderBik;

        return $this;
    }

    public function getSenderIban(): ?string
    {
        return $this->sender_iban;
    }

    public function setSenderIban(string $sender_iban): PsPay
    {
        $this->sender_iban = $sender_iban;

        return $this;
    }

    public function getRecipientIdn(): ?string
    {
        return $this->recipient_idn;
    }

    public function setRecipientIdn(string $recipient_idn): PsPay
    {
        $this->recipient_idn = $recipient_idn;

        return $this;
    }

    public function getRecipientName(): ?string
    {
        return $this->recipient_name;
    }

    public function setRecipientName(string $recipient_name): PsPay
    {
        $this->recipient_name = $recipient_name;

        return $this;
    }

    public function getRecipientBik(): ?string
    {
        return $this->recipient_bik;
    }

    public function setRecipientBik(string $recipient_bik): PsPay
    {
        $this->recipient_bik = $recipient_bik;

        return $this;
    }

    public function getRecipientIban(): ?string
    {
        return $this->recipientIban;
    }

    public function setRecipientIban(string $recipientIban): PsPay
    {
        $this->recipientIban = $recipientIban;

        return $this;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount): PsPay
    {
        $this->amount = $amount;

        return $this;
    }

    public function getDocNum(): ?string
    {
        return $this->doc_num;
    }

    public function setDocNum(string $doc_num): PsPay
    {
        $this->doc_num = $doc_num;

        return $this;
    }

    public function getDocDate(): ?\DateTimeInterface
    {
        return $this->doc_date;
    }

    /**
     * @param \DateTimeInterface|string $doc_date
     * @return PsPay
     */
    public function setDocDate($doc_date): PsPay
    {
        if($doc_date instanceof \DateTimeInterface)
            $this->doc_date = $doc_date;
        else
            $this->doc_date = SoapDateTime::create($doc_date);

        return $this;
    }

    public function getDocAssign(): ?string
    {
        return $this->doc_assign;
    }

    public function setDocAssign(?string $doc_assign): PsPay
    {
        $this->doc_assign = $doc_assign;

        return $this;
    }

    public function getKnp(): ?string
    {
        return $this->knp;
    }

    public function setKnp(string $knp): PsPay
    {
        $this->knp = $knp;

        return $this;
    }

    public function getDirect(): ?RefDirect
    {
        return $this->direct;
    }

    public function setDirect(?RefDirect $direct): PsPay
    {
        $this->direct = $direct;

        return $this;
    }

    public function getEccAcc(): ?RefEccAcc
    {
        return $this->eccAcc;
    }

    public function setEccAcc(?RefEccAcc $eccAcc): PsPay
    {
        $this->eccAcc = $eccAcc;

        return $this;
    }

    public function getStatus(): ?RefPaySt
    {
        return $this->refStatus;
    }

    public function setStatus(?RefPaySt $status): PsPay
    {
        $this->refStatus = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getJobWorkerId(): string
    {
        return $this->job_worker_id;
    }

    /**
     * @param string $job_worker_id
     * @return PsPay
     */
    public function setJobWorkerId(?string $job_worker_id): PsPay
    {
        $this->job_worker_id = $job_worker_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getJobStatus(): ?int
    {
        return $this->job_status;
    }

    /**
     * @param int $job_status
     * @return PsPay
     */
    public function setJobStatus(int $job_status): PsPay
    {
        $this->job_status = $job_status;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCrdate(): \DateTimeInterface
    {
        return $this->crdate;
    }

    /**
     * @param \DateTime $crdate
     * @return PsPay
     */
    public function setCrdate(?\DateTime $crdate): PsPay
    {
        $this->crdate = $crdate;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getJobStartTime(): \DateTimeInterface
    {
        return $this->job_start_time;
    }

    /**
     * @param \DateTime $job_start_time
     * @return PsPay
     */
    public function setJobStartTime(?\DateTime $job_start_time): PsPay
    {
        $this->job_start_time = $job_start_time;
        return $this;
    }

    /**
     * @return AbstractSysErrorLog[]|ArrayCollection
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param AbstractSysErrorLog[]|ArrayCollection|PersistentCollection $errors
     * @return PsPay
     */
    public function setErrors($errors): PsPay
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @return bool
     * @throws UnhandledException
     */
    public function isIncoming(): bool
    {
        if($this->direct==null)
            throw new UnhandledException('Не поределено направление');
        return $this->getDirect()->getCode() == RefDirect::IncomingCode;
    }

    /**
     * @return int
     */
    public function getApprovedUser(): ?int
    {
        return $this->approved_user;
    }

    /**
     * @param int $approved_user
     * @return PsPay
     */
    public function setApprovedUser(int $approved_user): PsPay
    {
        $this->approved_user = $approved_user;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getApprovedDate(): ?\DateTimeInterface
    {
        return $this->approved_date;
    }

    /**
     * @param \DateTimeInterface $approved_date
     * @return PsPay
     */
    public function setApprovedDate(\DateTimeInterface $approved_date): PsPay
    {
        $this->approved_date = $approved_date;
        return $this;
    }

    /**
     * @return string
     */
    public function getFaktSenderIdn():string
    {
        return $this->fakt_sender_idn;
    }

    /**
     * @param mixed $fakt_sender_idn
     * @return $this
     */
    public function setFaktSenderIdn($fakt_sender_idn): PsPay
    {
        $this->fakt_sender_idn = $fakt_sender_idn;
        return $this;
    }

    /**
     * @return PsPay
     */
    public function setCreateStatusCreated(): PsPay {
        $this->status = PsPay::STATUS_CREATED;
        return $this;
    }

    /**
     * @return PsPay
     */
    public function setCreatedStatusDeleted(): PsPay {
        $this->status = PsPay::STATUS_DELETED;
        return $this;
    }


    public function getCreatedStatus(): string {
        return  $this->status;
    }

    /**
     * @param string $message
     * @return PsPayErrorLog
     */
    public function addErrorMessage(string $message):PsPayErrorLog
    {
        $payError = new PsPayErrorLog();
        $payError->setErrorMessage($message);
        $payError->setPay($this);
        $this->getErrors()->add($payError);
        return $payError;
    }

    /**
     * @noinspection PhpUnusedPrivateMethodInspection
     * @param $value
     */
    private function set_pay_day($value)
    {
        $this->pay_day = SoapDateTime::create($value);
    }

    /**
     * @noinspection PhpUnusedPrivateMethodInspection
     * @param $value
     */
    private function set_doc_date($value)
    {
        $this->doc_date = SoapDateTime::create($value);
    }

    public function __construct()
    {
        $this->errors = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     */
    public function setDefaults()
    {
        if(null==$this->getJobStatus())
        {
            if(RefPaySt::UNPROCESSED_INCOMING==$this->getStatus()->getId())
                $this->setJobStatus(PsPay::JOB_STATUS_NOT_HANDLED);
            else
                $this->setJobStatus(PsPay::JOB_STATUS_ERROR);
        }
    }
}
