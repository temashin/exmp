<?php 

namespace App\Controller\HTTP\Rest;

use App\Entity\PsPay;
use App\Payments\DTO\ReassignPayment\PayItemRequest;
use App\Payments\Helpers\ReassignPaymentHelper;
use App\Payments\Validator\ReassignPayment;
use App\Repository\PsPayRepository;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ReassignPaymentController
{
    /**
     * @Route("/api/ReassignPayment/Execute", name="reassign_payment_execute")
     * @param Request $request
     * @param PsPayRepository $psPayRepository
     * @param EntityManagerInterface $em
     * @param ReassignPayment $validator
     * @param ReassignPaymentHelper $helper
     * @return Response
     */
    public function Execute(Request $request, 
                            PsPayRepository $psPayRepository,
                            EntityManagerInterface $em,
                            ReassignPayment $validator,
                            ReassignPaymentHelper $helper)
    {
        $payItem = PayItemRequest::__set_state(json_decode($request->getContent(), true));
        $psPay = $psPayRepository->find($payItem->ps_pay_id);
        if (is_null($psPay)) {
            return Response::create(json_encode(["status" => 1, "errors" => "Платеж не найден"]));
        }

        $errors = $validator->validate($psPay);
        if (count($errors) > 0) {
            return Response::create(json_encode(["status" => 1, "errors" => implode($errors)]));
        }

        $answer = [
            "status" => 0
        ];

        try {
            $em->beginTransaction();

            $reserved = $helper->createReserved($psPay);

            $answer["reserved_id"] = $reserved->getId();

            $em->persist($reserved);
            $em->flush();
            $em->commit();
        }catch(Exception $exp) {
            if ($em->getConnection()->isTransactionActive())
                $em->rollback();

            return Response::create(json_encode(array("status" => 1, "errors" => "Ошибка при сохранении изменений")));
        }

        return Response::create(json_encode($answer), Response::HTTP_OK);
    }

}
